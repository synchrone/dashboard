'use strict';

/* global Terminal */

// create main application module
var app = angular.module('Application', ['angular-md5', 'ui-notification']);

app.controller('TerminalController', ['$scope', '$timeout', '$location', 'Client', function ($scope, $timeout, $location, Client) {
    var search = decodeURIComponent(window.location.search).slice(1).split('&').map(function (item) { return item.split('='); }).reduce(function (o, k) { o[k[0]] = k[1]; return o; }, {});

    $scope.config = Client.getConfig();
    $scope.user = Client.getUserInfo();

    $scope.apps = [];
    $scope.selected = '';
    $scope.terminal = null;
    $scope.terminalSocket = null;
    $scope.restartAppBusy = false;
    $scope.appBusy = false;
    $scope.selectedAppInfo = null;
    $scope.schedulerTasks = [];

    $scope.downloadFile = {
        error: '',
        filePath: '',
        busy: false,

        downloadUrl: function () {
            if (!$scope.downloadFile.filePath) return '';

            var filePath = $scope.downloadFile.filePath.replace(/\/*\//g, '/');

            return Client.apiOrigin + '/api/v1/apps/' + $scope.selected.value + '/download?file=' + filePath + '&access_token=' + Client.getToken();
        },

        show: function () {
            $scope.downloadFile.busy = false;
            $scope.downloadFile.error = '';
            $scope.downloadFile.filePath = '';
            $('#downloadFileModal').modal('show');
        },

        submit: function () {
            $scope.downloadFile.busy = true;

            Client.checkDownloadableFile($scope.selected.value, $scope.downloadFile.filePath, function (error) {
                $scope.downloadFile.busy = false;

                if (error) {
                    $scope.downloadFile.error = 'The requested file does not exist.';
                    return;
                }

                // we have to click the link to make the browser do the download
                // don't know how to prevent the browsers
                $('#fileDownloadLink')[0].click();

                $('#downloadFileModal').modal('hide');
            });
        }
    };

    $scope.uploadProgress = {
        busy: false,
        total: 0,
        current: 0,

        show: function () {
            $scope.uploadProgress.total = 0;
            $scope.uploadProgress.current = 0;

            $('#uploadProgressModal').modal('show');
        },

        hide: function () {
            $('#uploadProgressModal').modal('hide');
        }
    };

    $scope.uploadFile = function () {
        var fileUpload = document.querySelector('#fileUpload');

        fileUpload.onchange = function (e) {
            if (e.target.files.length === 0) return;

            $scope.uploadProgress.busy = true;
            $scope.uploadProgress.show();

            Client.uploadFile($scope.selected.value, e.target.files[0], function progress(e) {
                $scope.uploadProgress.total = e.total;
                $scope.uploadProgress.current = e.loaded;
            }, function (error) {
                if (error) console.error(error);

                $scope.uploadProgress.busy = false;
                $scope.uploadProgress.hide();
            });
        };

        fileUpload.click();
    };

    $scope.usesAddon = function (addon) {
        if (!$scope.selected || !$scope.selected.addons) return false;
        return !!Object.keys($scope.selected.addons).find(function (a) { return a === addon; });
    };

    function reset() {
        if ($scope.terminal) {
            $scope.terminal.destroy();
            $scope.terminal = null;
        }

        if ($scope.terminalSocket) {
            $scope.terminalSocket = null;
        }

        $scope.selectedAppInfo = null;
    }

    $scope.restartApp = function () {
        $scope.restartAppBusy = true;
        var appId = $scope.selected.value;

        function waitUntilStopped(callback) {
            refreshApp(appId, function (error, result) {
                if (error) return callback(error);

                if (result.runState === 'stopped') return callback();
                setTimeout(waitUntilStopped.bind(null, callback), 2000);
            });
        }

        Client.stopApp(appId, function (error) {
            if (error) return console.error('Failed to stop app.', error);

            waitUntilStopped(function (error) {
                if (error) return console.error('Failed to get app status.', error);

                Client.startApp(appId, function (error) {
                    if (error) console.error('Failed to start app.', error);

                    $scope.restartAppBusy = false;
                });
            });
        });
    };

    $scope.repairApp = function () {
        $('#repairAppModal').modal('show');
    };

    $scope.repairAppBegin = function () {
        $scope.appBusy = true;

        function waitUntilInRepairState() {
            refreshApp($scope.selected.value, function (error, result) {
                if (error) return console.error('Failed to get app status.', error);

                if (result.installationState === 'installed') $scope.appBusy = false;
                else setTimeout(waitUntilInRepairState, 2000);
            });
        }

        Client.debugApp($scope.selected.value, true, function (error) {
            if (error) return console.error(error);

            $('#repairAppModal').modal('hide');

            waitUntilInRepairState();
        });
    };

    $scope.repairAppDone = function () {
        $scope.appBusy = true;

        function waitUntilInNormalState() {
            refreshApp($scope.selected.value, function (error, result) {
                if (error) return console.error('Failed to get app status.', error);

                if (result.installationState === 'installed') $scope.appBusy = false;
                else setTimeout(waitUntilInNormalState, 2000);
            });
        }

        Client.debugApp($scope.selected.value, false, function (error) {
            if (error) return console.error(error);

            waitUntilInNormalState();
        });
    };

    function createTerminalSocket() {
        try {
            // websocket cannot use relative urls
            var url = Client.apiOrigin.replace('https', 'wss') + '/api/v1/apps/' + $scope.selected.value + '/execws?tty=true&rows=' + $scope.terminal.rows + '&columns=' + $scope.terminal.cols + '&access_token=' + Client.getToken();
            $scope.terminalSocket = new WebSocket(url);
            $scope.terminal.attach($scope.terminalSocket);

            $scope.terminalSocket.onclose = function () {
                // retry in one second
                $scope.terminalReconnectTimeout = setTimeout(function () {
                    showTerminal(true);
                }, 1000);
            };

        } catch (e) {
            console.error(e);
        }
    }

    function refreshApp(id, callback) {
        Client.getApp(id, function (error, result) {
            if (error) return callback(error);

            $scope.selectedAppInfo = result;

            callback(null, result);
        });
    }

    function showTerminal(retry) {
        reset();

        if (!$scope.selected) return;

        var appId = $scope.selected.value;

        refreshApp(appId, function (error) {
            if (error) return console.error(error);

            var result = $scope.selectedAppInfo;

            // we expect this to be called _after_ a reconfigure was issued
            if (result.installationState === 'pending_configure') {
                $scope.appBusy = true;
            } else if (result.installationState === 'installed') {
                $scope.appBusy = false;
            }

            $scope.schedulerTasks = result.manifest.addons.scheduler ? Object.keys(result.manifest.addons.scheduler).map(function (k) { return { name: k, command: result.manifest.addons.scheduler[k].command }; }) : [];

            $scope.terminal = new Terminal();
            $scope.terminal.open(document.querySelector('#terminalContainer'), true);

            window.terminal = $scope.terminal;

            // Let the browser handle paste
            $scope.terminal.attachCustomKeyEventHandler(function (e) {
                if (e.key === 'v' && (e.ctrlKey || e.metaKey)) return false;
            });

            if (retry) $scope.terminal.writeln('Reconnecting...');
            else $scope.terminal.writeln('Connecting...');

            // we have to give it some time to setup the terminal to make it fit, there is no event unfortunately
            setTimeout(function () {
                if (!$scope.terminal) return;
                $scope.terminal.fit();

                // this is here so that the text wraps correctly after the fit!
                var YELLOW = '\u001b[33m'; // https://gist.github.com/dainkaplan/4651352
                var NC = '\u001b[0m';
                $scope.terminal.writeln(YELLOW + 'If you resize the browser window, press Ctrl+D to start a new session with the current size.' + NC);

                createTerminalSocket(); // create exec container after we fit() since we cannot resize exec container post-creation
            }, 1000);
        });
    }

    $scope.terminalInject = function (addon, extra) {
        if (!$scope.terminalSocket) return;

        var cmd;
        if (addon === 'mysql') cmd = 'mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQL_DATABASE}';
        else if (addon === 'postgresql') cmd = 'PGPASSWORD=${POSTGRESQL_PASSWORD} psql -h ${POSTGRESQL_HOST} -p ${POSTGRESQL_PORT} -U ${POSTGRESQL_USERNAME} -d ${POSTGRESQL_DATABASE}';
        else if (addon === 'mongodb') cmd = 'mongo -u "${MONGODB_USERNAME}" -p "${MONGODB_PASSWORD}" ${MONGODB_HOST}:${MONGODB_PORT}/${MONGODB_DATABASE}';
        else if (addon === 'redis') cmd = 'redis-cli -h "${REDIS_HOST}" -p "${REDIS_PORT}" -a "${REDIS_PASSWORD}"';
        else if (addon === 'scheduler' && extra) cmd = extra.command;

        if (!cmd) return;

        cmd += ' ';

        $scope.terminalSocket.send(cmd);
        $scope.terminal.focus();
    };

    // terminal right click handling
    $scope.terminalClear = function () {
        if (!$scope.terminal) return;
        $scope.terminal.clear();
        $scope.terminal.focus();
    };

    $scope.terminalCopy = function () {
        if (!$scope.terminal) return;

        // execCommand('copy') would copy any selection from the page, so do this only if terminal has a selection
        if (!$scope.terminal.getSelection()) return;

        document.execCommand('copy');
        $scope.terminal.focus();
    };

    $('.contextMenuBackdrop').on('click', function (e) {
        $('#terminalContextMenu').hide();
        $('.contextMenuBackdrop').hide();

        $scope.terminal.focus();
    });

    $('#terminalContainer').on('contextmenu', function (e) {
        if (!$scope.terminal) return true;

        e.preventDefault();

        $('.contextMenuBackdrop').show();
        $('#terminalContextMenu').css({
            display: 'block',
            left: e.pageX,
            top: e.pageY
        });

        return false;
    });

    window.addEventListener('resize', function (e) {
        if ($scope.terminal) $scope.terminal.fit();
    });

    Client.getStatus(function (error, status) {
        if (error) return $scope.error(error);

        if (!status.activated) {
            console.log('Not activated yet, closing or redirecting', status);
            window.close();
            window.location.href = '/';
            return;
        }

        // check version and force reload if needed
        if (!localStorage.version) {
            localStorage.version = status.version;
        } else if (localStorage.version !== status.version) {
            localStorage.version = status.version;
            window.location.reload(true);
        }

        console.log('Running terminal version ', localStorage.version);

        // get user profile as the first thing. this populates the "scope" and affects subsequent API calls
        Client.refreshUserInfo(function (error) {
            if (error) return $scope.error(error);

            Client.refreshConfig(function (error) {
                if (error) return $scope.error(error);

                refreshApp(search.id, function (error, app) {
                    $scope.selected = {
                        type: 'app',
                        value: app.id,
                        name: app.fqdn + ' (' + app.manifest.title + ')',
                        addons: app.manifest.addons
                    };

                    // now mark the Client to be ready
                    Client.setReady();

                    $scope.initialized = true;

                    showTerminal();
                });
            });
        });
    });

    // setup all the dialog focus handling
    ['downloadFileModal'].forEach(function (id) {
        $('#' + id).on('shown.bs.modal', function () {
            $(this).find("[autofocus]:first").focus();
        });
    });
}]);
