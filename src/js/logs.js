'use strict';

/* global moment */

// create main application module
var app = angular.module('Application', ['angular-md5', 'ui-notification']);

app.controller('LogsController', ['$scope', '$timeout', '$location', 'Client', function ($scope, $timeout, $location, Client) {
    var search = decodeURIComponent(window.location.search).slice(1).split('&').map(function (item) { return item.split('='); }).reduce(function (o, k) { o[k[0]] = k[1]; return o; }, {});

    $scope.initialized = false;
    $scope.client = Client;
    $scope.selected = '';
    $scope.activeEventSource = null;
    $scope.lines = 100;
    $scope.selectedAppInfo = null;

    $scope.error = function (error) {
        console.error(error);
        window.location.href = '/error.html';
    };

    function ab2str(buf) {
        return String.fromCharCode.apply(null, new Uint16Array(buf));
    }

    $scope.clear = function () {
        var logViewer = $('.logs-container');
        logViewer.empty();
    };

    function showLogs() {
        if (!$scope.selected) return;

        var func = $scope.selected.type === 'platform' ? Client.getPlatformLogs : Client.getAppLogs;
        func($scope.selected.value, true, $scope.lines, function handleLogs(error, result) {
            if (error) return console.error(error);

            $scope.activeEventSource = result;
            result.onmessage = function handleMessage(message) {
                var data;

                try {
                    data = JSON.parse(message.data);
                } catch (e) {
                    return console.error(e);
                }

                // check if we want to auto scroll (this is before the appending, as that skews the check)
                var tmp = $('.logs-container');
                var autoScroll = tmp[0].scrollTop > (tmp[0].scrollHeight - tmp.innerHeight() - 24);

                var logLine = $('<div class="log-line">');
                var timeString = moment.utc(data.realtimeTimestamp/1000).format('MMM DD HH:mm:ss');
                logLine.html('<span class="time">' + timeString + ' </span>' + window.ansiToHTML(typeof data.message === 'string' ? data.message : ab2str(data.message)));
                tmp.append(logLine);

                if (autoScroll) tmp[0].lastChild.scrollIntoView({ behavior: 'instant', block: 'end' });
            };
        });
    }

    function loadId(id, callback) {
        // Add built-in log types for now
        var BUILT_IN_LOGS = [
            { name: 'Box', type: 'platform', value: 'box', url: Client.makeURL('/api/v1/cloudron/logs/box') },
            { name: 'Mail', type: 'platform', value: 'mail', url: Client.makeURL('/api/v1/cloudron/logs/mail') },
            { name: 'Backup', type: 'platform', value: 'backup', url: Client.makeURL('/api/v1/cloudron/logs/backup') }
        ];

        $scope.selected = BUILT_IN_LOGS.find(function (e) { return e.value === id; });
        if ($scope.selected) return callback();

        Client.getApp(id, function (error, app) {
            if (error) return callback(error);

            $scope.selectedAppInfo = app;

            $scope.selected = {
                type: 'app',
                value: app.id,
                name: app.fqdn + ' (' + app.manifest.title + ')',
                url: Client.makeURL('/api/v1/apps/' + app.id + '/logs'),
                addons: app.manifest.addons
            };

            callback();
        });
    }

    Client.getStatus(function (error, status) {
        if (error) return $scope.error(error);

        if (!status.activated) {
            console.log('Not activated yet, redirecting', status);
            window.location.href = '/';
            return;
        }

        // check version and force reload if needed
        if (!localStorage.version) {
            localStorage.version = status.version;
        } else if (localStorage.version !== status.version) {
            localStorage.version = status.version;
            window.location.reload(true);
        }

        console.log('Running log version ', localStorage.version);

        // get user profile as the first thing. this populates the "scope" and affects subsequent API calls
        Client.refreshUserInfo(function (error) {
            if (error) return $scope.error(error);

            Client.refreshConfig(function (error) {
                if (error) return $scope.error(error);

                loadId(search.id, function (error) {
                    if (error) return $scope.error(error);

                    // now mark the Client to be ready
                    Client.setReady();

                    $scope.initialized = true;

                    showLogs();
                });
            });
        });
    });
}]);
