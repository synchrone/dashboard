'use strict';

angular.module('Application').controller('ActivityController', ['$scope', '$location', 'Client', function ($scope, $location, Client) {
    Client.onReady(function () { if (!Client.getUserInfo().admin) $location.path('/'); });

    $scope.config = Client.getConfig();

    $scope.busy = false;
    $scope.eventLogs = [];
    $scope.activeEventLog = null;

    // TODO sync this with the eventlog filter
    $scope.actions = [
        { name: '-- All app events --', value: 'app.' },
        { name: '-- All user events --', value: 'user.' },
        { name: 'app.configure', value: 'app.configure' },
        { name: 'app.install', value: 'app.install' },
        { name: 'app.restore', value: 'app.restore' },
        { name: 'app.uninstall', value: 'app.uninstall' },
        { name: 'app.update', value: 'app.update' },
        { name: 'app.login', value: 'app.login' },
        { name: 'backup.cleanup', value: 'backup.cleanup' },
        { name: 'backup.finish', value: 'backup.finish' },
        { name: 'backup.start', value: 'backup.start' },
        { name: 'certificate.renew', value: 'certificate.renew' },
        { name: 'settings.climode', value: 'settings.climode' },
        { name: 'cloudron.activate', value: 'cloudron.activate' },
        { name: 'cloudron.start', value: 'cloudron.start' },
        { name: 'cloudron.update', value: 'cloudron.update' },
        { name: 'user.add', value: 'user.add' },
        { name: 'user.login', value: 'user.login' },
        { name: 'user.remove', value: 'user.remove' },
        { name: 'user.transfer', value: 'user.transfer' },
        { name: 'user.update', value: 'user.update' }
    ];

    $scope.pageItemCount = [
        { name: 'Show 20 per page', value: 20 },
        { name: 'Show 50 per page', value: 50 },
        { name: 'Show 100 per page', value: 100 }
    ];

    $scope.currentPage = 1;
    $scope.pageItems = $scope.pageItemCount[0];
    $scope.action = '';
    $scope.selectedActions = [];
    $scope.search = '';

    function fetchEventLogs() {
        $scope.busy = true;
        var actions = $scope.selectedActions.map(function (a) { return a.value; }).join(', ');

        Client.getEventLogs(actions, $scope.search || null, $scope.currentPage, $scope.pageItems.value, function (error, result) {
            $scope.busy = false;

            if (error) return console.error(error);

            $scope.eventLogs = result;
        });
    }

    $scope.showNextPage = function () {
        $scope.currentPage++;
        fetchEventLogs();
    };

    $scope.showPrevPage = function () {
        if ($scope.currentPage > 1) $scope.currentPage--;
        else $scope.currentPage = 1;

        fetchEventLogs();
    };

    $scope.updateFilter = function (fresh) {
        if (fresh) $scope.currentPage = 1;
        fetchEventLogs();
    };

    $scope.showEventLogDetails = function (eventLog) {
        if ($scope.activeEventLog === eventLog) $scope.activeEventLog = null;
        else $scope.activeEventLog = eventLog;
    };

    Client.onReady(function () {
        fetchEventLogs();
    });

    $('.modal-backdrop').remove();
}]);
