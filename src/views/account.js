'use strict';

/* global asyncForEach:false */

angular.module('Application').controller('AccountController', ['$scope', 'Client', function ($scope, Client) {
    $scope.user = Client.getUserInfo();
    $scope.config = Client.getConfig();

    $scope.activeTokens = 0;
    $scope.activeClients = [];
    $scope.webadminClient = {};
    $scope.apiClient = {};

    $scope.twoFactorAuthentication = {
        busy: false,
        error: null,
        password: '',
        totpToken: '',
        secret: '',
        qrcode: '',

        reset: function () {
            $scope.twoFactorAuthentication.busy = false;
            $scope.twoFactorAuthentication.error = null;
            $scope.twoFactorAuthentication.password = '';
            $scope.twoFactorAuthentication.totpToken = '';
            $scope.twoFactorAuthentication.secret = '';
            $scope.twoFactorAuthentication.qrcode = '';

            $scope.twoFactorAuthenticationEnableForm.$setUntouched();
            $scope.twoFactorAuthenticationEnableForm.$setPristine();
            $scope.twoFactorAuthenticationDisableForm.$setUntouched();
            $scope.twoFactorAuthenticationDisableForm.$setPristine();
        },

        show: function () {
            $scope.twoFactorAuthentication.reset();

            if ($scope.user.twoFactorAuthenticationEnabled) {
                $('#twoFactorAuthenticationDisableModal').modal('show');
            } else {
                $('#twoFactorAuthenticationEnableModal').modal('show');

                Client.setTwoFactorAuthenticationSecret(function (error, result) {
                    if (error) return console.error(error);

                    $scope.twoFactorAuthentication.secret = result.secret;
                    $scope.twoFactorAuthentication.qrcode = result.qrcode;
                });
            }
        },

        enable: function() {
            $scope.twoFactorAuthentication.busy = true;

            Client.enableTwoFactorAuthentication($scope.twoFactorAuthentication.totpToken, function (error) {
                $scope.twoFactorAuthentication.busy = false;

                if (error) {
                    $scope.twoFactorAuthentication.error = error.message;

                    $scope.twoFactorAuthentication.totpToken = '';
                    $scope.twoFactorAuthenticationEnableForm.totpToken.$setPristine();
                    $('#twoFactorAuthenticationTotpTokenInput').focus();

                    return;
                }

                Client.refreshUserInfo();

                $('#twoFactorAuthenticationEnableModal').modal('hide');
            });
        },

        disable: function () {
            $scope.twoFactorAuthentication.busy = true;

            Client.disableTwoFactorAuthentication($scope.twoFactorAuthentication.password, function (error) {
                $scope.twoFactorAuthentication.busy = false;

                if (error) {
                    $scope.twoFactorAuthentication.error = error.message;

                    $scope.twoFactorAuthentication.password = '';
                    $scope.twoFactorAuthenticationDisableForm.password.$setPristine();
                    $('#twoFactorAuthenticationPasswordInput').focus();

                    return;
                }

                Client.refreshUserInfo();

                $('#twoFactorAuthenticationDisableModal').modal('hide');
            });
        }
    };

    $scope.passwordchange = {
        busy: false,
        error: {},
        password: '',
        newPassword: '',
        newPasswordRepeat: '',

        reset: function () {
            $scope.passwordchange.error.password = null;
            $scope.passwordchange.error.newPassword = null;
            $scope.passwordchange.error.newPasswordRepeat = null;
            $scope.passwordchange.password = '';
            $scope.passwordchange.newPassword = '';
            $scope.passwordchange.newPasswordRepeat = '';

            $scope.passwordChangeForm.$setUntouched();
            $scope.passwordChangeForm.$setPristine();
        },

        show: function () {
            $scope.passwordchange.reset();
            $('#passwordChangeModal').modal('show');
        },

        submit: function () {
            $scope.passwordchange.error.password = null;
            $scope.passwordchange.error.newPassword = null;
            $scope.passwordchange.error.newPasswordRepeat = null;
            $scope.passwordchange.busy = true;

            Client.changePassword($scope.passwordchange.password, $scope.passwordchange.newPassword, function (error) {
                $scope.passwordchange.busy = false;

                if (error) {
                    if (error.statusCode === 403) {
                        $scope.passwordchange.error.password = true;
                        $scope.passwordchange.password = '';
                        $('#inputPasswordChangePassword').focus();
                        $scope.passwordChangeForm.password.$setPristine();
                    } else if (error.statusCode === 400) {
                        $scope.passwordchange.error.newPassword = error.message;
                        $scope.passwordchange.newPassword = '';
                        $scope.passwordchange.newPasswordRepeat = '';
                        $scope.passwordChangeForm.newPassword.$setPristine();
                        $scope.passwordChangeForm.newPasswordRepeat.$setPristine();
                        $('#inputPasswordChangeNewPassword').focus();
                    } else {
                        console.error('Unable to change password.', error);
                    }
                    return;
                }

                $scope.passwordchange.reset();
                $('#passwordChangeModal').modal('hide');
            });
        }
    };

    $scope.emailchange = {
        busy: false,
        error: {},
        email: '',

        reset: function () {
            $scope.emailchange.busy = false;
            $scope.emailchange.error.email = null;
            $scope.emailchange.email = '';

            $scope.emailChangeForm.$setUntouched();
            $scope.emailChangeForm.$setPristine();
        },

        show: function () {
            $scope.emailchange.reset();
            $('#emailChangeModal').modal('show');
        },

        submit: function () {
            $scope.emailchange.error.email = null;
            $scope.emailchange.busy = true;

            var data = {
                email: $scope.emailchange.email
            };

            Client.updateProfile(data, function (error) {
                $scope.emailchange.busy = false;

                if (error) {
                    if (error.statusCode === 409) {
                        $scope.emailchange.error.email = 'Email already taken';
                        $scope.emailChangeForm.email.$setPristine();
                        $('#inputEmailChangeEmail').focus();
                    } else {
                        console.error('Unable to change email.', error);
                    }
                    return;
                }

                // update user info in the background
                Client.refreshUserInfo();

                $scope.emailchange.reset();
                $('#emailChangeModal').modal('hide');
            });
        }
    };

    $scope.fallbackEmailChange = {
        busy: false,
        error: {},
        email: '',

        reset: function () {
            $scope.fallbackEmailChange.busy = false;
            $scope.fallbackEmailChange.error.email = null;
            $scope.fallbackEmailChange.email = '';

            $scope.fallbackEmailChangeForm.$setUntouched();
            $scope.fallbackEmailChangeForm.$setPristine();
        },

        show: function () {
            $scope.fallbackEmailChange.reset();
            $('#fallbackEmailChangeModal').modal('show');
        },

        submit: function () {
            $scope.fallbackEmailChange.error.email = null;
            $scope.fallbackEmailChange.busy = true;

            var data = {
                fallbackEmail: $scope.fallbackEmailChange.email
            };

            Client.updateProfile(data, function (error) {
                $scope.fallbackEmailChange.busy = false;

                if (error) return console.error('Unable to change fallback email.', error);

                // update user info in the background
                Client.refreshUserInfo();

                $scope.fallbackEmailChange.reset();
                $('#fallbackEmailChangeModal').modal('hide');
            });
        }
    };

    $scope.displayNameChange = {
        busy: false,
        error: {},
        displayName: '',

        reset: function () {
            $scope.displayNameChange.busy = false;
            $scope.displayNameChange.error.displayName = null;
            $scope.displayNameChange.displayName = '';

            $scope.displayNameChangeForm.$setUntouched();
            $scope.displayNameChangeForm.$setPristine();
        },

        show: function () {
            $scope.displayNameChange.reset();
            $scope.displayNameChange.displayName = $scope.user.displayName;
            $('#displayNameChangeModal').modal('show');
        },

        submit: function () {
            $scope.displayNameChange.error.displayName = null;
            $scope.displayNameChange.busy = true;

            var user = {
                displayName: $scope.displayNameChange.displayName
            };

            Client.updateProfile(user, function (error) {
                $scope.displayNameChange.busy = false;

                if (error) {
                    if (error.statusCode === 400) {
                        $scope.displayNameChange.error.displayName = 'Invalid display name';
                        $scope.displayNameChangeForm.email.$setPristine();
                        $('#inputDisplayNameChangeDisplayName').focus();
                    } else {
                        console.error('Unable to change email.', error);
                    }
                    return;
                }

                // update user info in the background
                Client.refreshUserInfo();

                $scope.displayNameChange.reset();
                $('#displayNameChangeModal').modal('hide');
            });
        }
    };

    $scope.tokenAdd = {
        token: {},
        tokenName: '',
        busy: false,
        error: {},

        reset: function () {
            $scope.tokenAdd.busy = false;
            $scope.tokenAdd.token = {};
            $scope.tokenAdd.error.tokenName = null;
            $scope.tokenAdd.tokenName = '';

            $scope.tokenAddForm.$setUntouched();
            $scope.tokenAddForm.$setPristine();
        },

        show: function (client) {
            $scope.tokenAdd.reset();
            $('#tokenAddModal').modal('show');
        },

        submit: function (client) {
            $scope.tokenAdd.busy = true;
            $scope.tokenAdd.token = {};

            var expiresAt = Date.now() + 100 * 365 * 24 * 60 * 60 * 1000;   // ~100 years from now

            Client.createTokenByClientId(client.id, '*' /* scope */, expiresAt, $scope.tokenAdd.tokenName, function (error, result) {
                if (error) {
                    if (error.statusCode === 400) {
                        $scope.tokenAdd.error.tokenName = 'Invalid token name';
                        $scope.tokenAddForm.tokenName.$setPristine();
                        $('#inputTokenAddName').focus();
                    } else {
                        console.error('Unable to create token.', error);
                    }
                    return;
                }

                $scope.tokenAdd.busy = false;
                $scope.tokenAdd.token = result;

                refreshClientTokens(client);
            });
        }
    };

    $scope.removeToken = function (client, token) {
        Client.delToken(client.id, token.accessToken, function (error) {
            if (error) console.error(error);

            refreshClientTokens(client);
        });
    };

    function revokeTokensByClient(client, callback) {
        Client.delTokensByClientId(client.id, function (error) {
            if (error) console.error(error);
            callback();
        });
    }

    $scope.revokeTokens = function () {
        asyncForEach($scope.activeClients, revokeTokensByClient, function () {

            // now kill this session if exists
            if (!$scope.webadminClient || !$scope.webadminClient.id) return;

            revokeTokensByClient($scope.webadminClient, function () {
                // we should be logged out by now
            });
        });
    };

    function refreshClientTokens(client, callback) {
        Client.getTokensByClientId(client.id, function (error, result) {
            if (error) console.error(error);

            client.activeTokens = result || [];

            if (callback) callback();
        });
    }

    Client.onReady(function () {
        Client.getOAuthClients(function (error, activeClients) {
            if (error) return console.error(error);

            asyncForEach(activeClients, refreshClientTokens, function () {
                $scope.webadminClient = activeClients.filter(function (c) { return c.id === 'cid-webadmin'; })[0];
                $scope.apiClient = activeClients.filter(function (c) { return c.id === 'cid-sdk'; })[0];

                activeClients = activeClients.filter(function (c) { return c.activeTokens.length > 0; });

                $scope.activeClients = activeClients.filter(function (c) { return c.id !== 'cid-sdk' && c.id !== 'cid-webadmin'; });

                $scope.activeTokenCount = $scope.activeClients.reduce(function (prev, cur) { return prev + cur.activeTokens.length; }, 0);
                $scope.activeTokenCount += $scope.webadminClient ? $scope.webadminClient.activeTokens.length : 0;
            });
        });
    });

    // setup all the dialog focus handling
    ['passwordChangeModal', 'emailChangeModal', 'fallbackEmailChangeModal', 'displayNameChangeModal', 'twoFactorAuthenticationEnableModal', 'twoFactorAuthenticationDisableModal', 'tokenAddModal'].forEach(function (id) {
        $('#' + id).on('shown.bs.modal', function () {
            $(this).find("[autofocus]:first").focus();
        });
    });

    $('.modal-backdrop').remove();
}]);
