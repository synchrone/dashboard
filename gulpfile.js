/* jslint node:true */

'use strict';

var argv = require('yargs').argv,
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    cssnano = require('gulp-cssnano'),
    ejs = require('gulp-ejs'),
    gulp = require('gulp'),
    rimraf = require('rimraf'),
    sass = require('gulp-sass'),
    serve = require('gulp-serve'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify');

gulp.task('3rdparty', function () {
    gulp.src([
        'src/3rdparty/**/*.js',
        'src/3rdparty/**/*.map',
        'src/3rdparty/**/*.css',
        'src/3rdparty/**/*.otf',
        'src/3rdparty/**/*.eot',
        'src/3rdparty/**/*.svg',
        'src/3rdparty/**/*.gif',
        'src/3rdparty/**/*.ttf',
        'src/3rdparty/**/*.woff',
        'src/3rdparty/**/*.woff2'
        ])
        .pipe(gulp.dest('dist/3rdparty/'));

    gulp.src('node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js')
        .pipe(gulp.dest('dist/3rdparty/js'));
});


// --------------
// JavaScript
// --------------

if (argv.help || argv.h) {
    console.log('Supported arguments for "gulp develop":');
    console.log(' --client-id <clientId>');
    console.log(' --client-secret <clientSecret>');
    console.log(' --api-origin <cloudron api uri>');
    console.log(' --revision <revision>');

    process.exit(1);
}

gulp.task('js', ['js-index', 'js-logs', 'js-terminal', 'js-setup', 'js-setupdns', 'js-restore', 'js-update'], function () {});

var oauth = {
    clientId: argv.clientId || 'cid-webadmin',
    clientSecret: argv.clientSecret || 'unused',
    apiOrigin: argv.apiOrigin || '',
};

var revision = argv.revision || '';

console.log();
console.log('Using OAuth credentials:');
console.log(' ClientId:      %s', oauth.clientId);
console.log(' ClientSecret:  %s', oauth.clientSecret);
console.log(' Cloudron API:  %s', oauth.apiOrigin || 'default');
console.log();
console.log('Building for revision: %s', revision);
console.log();


gulp.task('js-index', function () {
    // needs special treatment for error handling
    var uglifyer = uglify();
    uglifyer.on('error', function (error) {
        console.error(error);
    });

    gulp.src([
        'src/js/index.js',
        'src/js/client.js',
        'src/js/appstore.js',
        'src/js/main.js',
        'src/views/*.js'
        ])
        .pipe(ejs({ oauth: oauth, revision: revision }, {}, { ext: '.js' }))
        .pipe(sourcemaps.init())
        .pipe(concat('index.js', { newLine: ';' }))
        .pipe(uglifyer)
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-logs', function () {
    // needs special treatment for error handling
    var uglifyer = uglify();
    uglifyer.on('error', function (error) {
        console.error(error);
    });

    gulp.src(['src/js/logs.js', 'src/js/client.js'])
        .pipe(ejs({ oauth: oauth }, {}, { ext: '.js' }))
        .pipe(sourcemaps.init())
        .pipe(concat('logs.js', { newLine: ';' }))
        .pipe(uglifyer)
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-terminal', function () {
    // needs special treatment for error handling
    var uglifyer = uglify();
    uglifyer.on('error', function (error) {
        console.error(error);
    });

    gulp.src(['src/js/terminal.js', 'src/js/client.js'])
        .pipe(ejs({ oauth: oauth }, {}, { ext: '.js' }))
        .pipe(sourcemaps.init())
        .pipe(concat('terminal.js', { newLine: ';' }))
        .pipe(uglifyer)
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-setup', function () {
    // needs special treatment for error handling
    var uglifyer = uglify();
    uglifyer.on('error', function (error) {
        console.error(error);
    });

    gulp.src(['src/js/setup.js', 'src/js/client.js'])
        .pipe(ejs({ oauth: oauth }, {}, { ext: '.js' }))
        .pipe(sourcemaps.init())
        .pipe(concat('setup.js', { newLine: ';' }))
        .pipe(uglifyer)
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-setupdns', function () {
    // needs special treatment for error handling
    var uglifyer = uglify();
    uglifyer.on('error', function (error) {
        console.error(error);
    });

    gulp.src(['src/js/setupdns.js', 'src/js/client.js'])
        .pipe(ejs({ oauth: oauth }, {}, { ext: '.js' }))
        .pipe(sourcemaps.init())
        .pipe(concat('setupdns.js', { newLine: ';' }))
        .pipe(uglifyer)
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-restore', function () {
    // needs special treatment for error handling
    var uglifyer = uglify();
    uglifyer.on('error', function (error) {
        console.error(error);
    });

    gulp.src(['src/js/restore.js', 'src/js/client.js'])
        .pipe(ejs({ oauth: oauth }, {}, { ext: '.js' }))
        .pipe(sourcemaps.init())
        .pipe(concat('restore.js', { newLine: ';' }))
        .pipe(uglifyer)
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});


gulp.task('js-update', function () {
    // needs special treatment for error handling
    var uglifyer = uglify();
    uglifyer.on('error', function (error) {
        console.error(error);
    });

    gulp.src(['src/js/update.js'])
        .pipe(sourcemaps.init())
        .pipe(uglifyer)
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});


// --------------
// HTML
// --------------

gulp.task('html', ['html-views', 'html-templates'], function () {
    return gulp.src('src/*.html').pipe(ejs({ revision: revision }, {}, { ext: '.html' })).pipe(gulp.dest('dist'));
});

gulp.task('html-views', function () {
    return gulp.src('src/views/**/*.html').pipe(gulp.dest('dist/views'));
});

gulp.task('html-templates', function () {
    return gulp.src('src/templates/**/*.html').pipe(gulp.dest('dist/templates'));
});

// --------------
// CSS
// --------------

gulp.task('css', function () {
    return gulp.src('src/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ includePaths: ['node_modules/bootstrap-sass/assets/stylesheets/'] }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cssnano())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist'));
});

gulp.task('images', function () {
    return gulp.src('src/img/**')
        .pipe(gulp.dest('dist/img'));
});

// --------------
// Utilities
// --------------

gulp.task('watch', ['default'], function () {
    gulp.watch(['src/*.scss'], ['css']);
    gulp.watch(['src/img/*'], ['images']);
    gulp.watch(['src/**/*.html'], ['html']);
    gulp.watch(['src/views/*.html'], ['html-views']);
    gulp.watch(['src/templates/*.html'], ['html-templates']);
    gulp.watch(['src/js/update.js'], ['js-update']);
    gulp.watch(['src/js/setup.js', 'src/js/client.js'], ['js-setup']);
    gulp.watch(['src/js/setupdns.js', 'src/js/client.js'], ['js-setupdns']);
    gulp.watch(['src/js/restore.js', 'src/js/client.js'], ['js-restore']);
    gulp.watch(['src/js/logs.js', 'src/js/client.js'], ['js-logs']);
    gulp.watch(['src/js/terminal.js', 'src/js/client.js'], ['js-terminal']);
    gulp.watch(['src/js/index.js', 'src/js/client.js', 'src/js/appstore.js', 'src/js/main.js', 'src/views/*.js'], ['js-index']);
    gulp.watch(['src/3rdparty/**/*'], ['3rdparty']);
});

gulp.task('clean', function () {
    rimraf.sync('dist');
});

gulp.task('default', ['clean', 'html', 'js', '3rdparty', 'images', 'css'], function () {});

gulp.task('develop', ['watch'], serve({ root: 'dist', port: 4000 }));
